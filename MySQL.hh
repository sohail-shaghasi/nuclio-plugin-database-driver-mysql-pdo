<?hh //partial
/*****************************************************************************
 *                                                                           *
 * This file is part of the Nuclio framework.                                *
 *                                                                           *
 * (c) Zinios <support@zinios.com>                                           *
 *                                                                           *
 * For the full copyright and license information, please view the LICENSE   *
 * file that was distributed with this source code.                          *
 *                                                                           *
 *****************************************************************************/
namespace nuclio\plugin\database\driver\mysqlPDO
{
	use nuclio\core\ClassManager;
	use nuclio\plugin\database\
	{
		exception\DatabaseException,
		common\PdoFunctions,
		common\DBRecord,
		common\DBVector,
		common\DBQuery,
		common\DBFields,
		common\DBCondition,
		orm\Model
	};
	use \PDO;
	use \PDOStatement;
	use \PDOException;

	<<provides('database::mysqlPDO')>>
	class MySQL extends PdoFunctions
	{
		const string DEFAULT_TYPE				='TEXT';
		const string DEFAULT_PRIMARY_ID_FIELD	='id';
		
		public static function getInstance(/* HH_FIXME[4033] */...$args):MySQL
		{
			$instance=ClassManager::getClassInstance(self::class);
			return ($instance instanceof self)?$instance:new self(...$args);
		}
		
		private Map<string,string> $settings=Map{};

		public function __construct(Map<string,string> $settings=Map{})
		{
			$this->settings=$settings;
			$this->connect();
			// $this->pdoStatement=$this->connection->exec('SET CHARACTER SET utf8');
			// parent::__construct();
		}
		
		/**
		 * Make Connetion String for Using MySQL
		 */
		private function connect():void
		{
			$database	=$this->settings->get('database');
			$host		=$this->settings->get('host');
			$username	=$this->settings->get('username');
			$password	=$this->settings->get('password');
			$dsn		='mysql:dbname='.$database.';host='.$host;
			try
			{
				$this->connection=new PDO($dsn,$username,$password);
			}
			catch (PDOException $e)
			{
				throw new DatabaseException($e->getMessage());
			}
		}
		
		public function collectionExists(string $target):bool
		{
			$query=<<<SQL
			SELECT *
			FROM information_schema.tables
			WHERE table_schema=?
			AND table_name=?
			LIMIT 1;
SQL;
			$statement=$this->connection->prepare($query);
			$statement->bindParam(1,$this->settings->get('database'));
			$statement->bindParam(2,$target);
			$statement->execute();
			$result=$statement->fetchAll();
			return (bool)count($result);
		}
		
		public function columnExists(Model $model,string $column):bool
		{
			$query=<<<SQL
			SELECT * 
			FROM information_schema.COLUMNS 
			WHERE TABLE_SCHEMA=?
			AND TABLE_NAME=?
			AND COLUMN_NAME=?;
SQL;
			$statement=$this->connection->prepare($query);
			$statement->bindParam(1,$this->settings->get('database'));
			$statement->bindParam(2,$model->getCollection());
			$statement->bindParam(3,$column);
			$statement->execute();
			$result=$statement->fetchAll();
			return (bool)count($result);
		}
		
		public function createCollection(Model $model, array<mixed,mixed> $options=[]):bool
		{
			$fields			=$model->getFields();
			$annotations	=$model->getAnnotations();
			$collectionName	=$model->getCollection();
			$columns		=Vector{};
			$primaryKey		=null;
			$indexes		=Vector{};
			
			if ($annotations->containsKey('class'))
			{
				$classAnnotations=$annotations->get('class');
				//TODO
				
			}
			foreach ($annotations as $field=>$annotation)
			{
				if ($field!=='class')
				{
					if ($annotation->containsKey('id'))
					{
						$primaryKey=sprintf('PRIMARY KEY(`%s`)',$field);
					}
					if (!$annotation->containsKey('relate'))
					{
						$columns->add($this->buildColumn($field,$annotation));
					}
					else
					{
						$relationship=$annotation->get('relate');
						if (!$relationship instanceof Vector)
						{
							$relationship=Vector{$relationship,self::DEFAULT_PRIMARY_ID_FIELD};
						}
						$columns->add($this->buildColumn($field,Map{'type'=>'int','length'=>10}));
						// $columns->add($this->buildColumn('nnb_orm_'.$field,Map{'type'=>'text'}));
						$model=ClassManager::getClassInstance($relationship[0]);
						$refCollection=$model->getCollection();
						
						$foreignKey=<<<FOREIGN_KEY
						INDEX %s (%s),
						FOREIGN KEY (%s)
						REFERENCES %s(%s)
						ON DELETE CASCADE
FOREIGN_KEY;
						$indexes->add
						(
							sprintf
							(
								$foreignKey,
								$refCollection.'_index',
								$field,
								$field,
								$refCollection,
								$relationship[1]
							)
						);
					}
				}
			}
			$columns=implode(",\n",$columns);
			if (count($indexes))
			{
				$indexes=",\n".implode(",\n",$indexes);
			}
			else
			{
				$indexes='';
			}
			$query=<<<SQL
			CREATE TABLE `{$collectionName}`
			(
				{$columns},
				{$primaryKey}{$indexes}
			) ENGINE=InnoDB DEFAULT CHARSET=utf8;
SQL;
			$statement=$this->connection->prepare($query);
			if (!$statement->execute())
			{
				throw new DatabaseException(sprintf('Error creating database table. Error Info: %s',print_r($statement->errorInfo(),true)));
			}
			return true;
		}
		
		public function alterColumn(Model $model,string $column,string $modification):bool
		{
			$collectionName	=$model->getCollection();
			$annotations	=$model->getAnnotations();
			
			$query=null;
			switch ($modification)
			{
				case 'add':
				{
					$annotation=$annotations->get($column);
					if ($annotation)
					{
						$query=sprintf
						(
							'ALTER TABLE `%s` ADD COLUMN %s',
							$collectionName,
							$this->buildColumn($column,$annotation)
						);
					}
					else
					{
						throw new DatabaseException(sprintf('Unable to alter column "%s" because no annotations have been defined in the model.',$column));
					}
					break;
				}
				case 'remove':
				{
					$query=sprintf
					(
						'ALTER TABLE `%s` CHANGE `%s` `__%s` TEXT NULL NULL',
						$collectionName,
						$column,
						$column
					);
					break;
				}
				case 'restore':
				{
					$annotation=$annotations->get($column);
					if ($annotation)
					{
						$query=sprintf
						(
							'ALTER TABLE `%s` CHANGE `__%s` %s',
							$collectionName,
							$column,
							$this->buildColumn($column,$annotation)
						);
					}
					else
					{
						throw new DatabaseException(sprintf('Unable to alter column "%s" because no annotations have been defined in the model.',$column));
					}
					break;
				}
			}
			if (!is_null($query))
			{
				$statement=$this->connection->prepare($query);
				if (!$statement->execute())
				{
					throw new DatabaseException(sprintf('Error modifying database table. Error Info: %s',print_r($statement->errorInfo(),true)));
				}
			}
			return true;
		}
		
		public function buildColumn(string $name,Map<string,mixed> $annotations):string
		{
			$length		='';
			$type		=self::DEFAULT_TYPE;
			$nullable	='NOT NULL';
			$default	='DEFAULT "%s"';
			
			if (!$annotations->containsKey('id'))
			{
				if ($annotations->containsKey('type'))
				{
					$type=strtoupper($annotations->get('type'));
				}
				if ($annotations->containsKey('length'))
				{
					$length='('.$annotations->get('length').')';
				}
				if ($annotations->containsKey('nullable'))
				{
					$nullable='NULL';
				}
				if ($annotations->containsKey('default'))
				{
					$default=sprintf($default,$annotations->get('default'));
				}
				else
				{
					$default='';
				}
				return sprintf
				(
					'`%s` %s%s %s %s',
					$name,
					$type,
					$length,
					$nullable,
					$default
				);
			}
			else
			{
				$length=10;
				if ($annotations->containsKey('length'))
				{
					$length=$annotations->get('length');
				}
				return sprintf
				(
					'`%s` INT(%s) NOT NULL AUTO_INCREMENT',
					$name,
					$length
				);
			}
		}
	}
}
